import React, {useEffect} from 'react'

import {fetchEmployees, metEmployee} from "../redux/actions";
import Employees from "../components/Employees/"
import {useDispatch, useSelector} from "react-redux";


const MetEmployeesPage = props => {
    const {employees, isFetching, metEmployee} = props;
    return (
        <div className="App">
            {employees.length ? (
                <div className="container">
                    <div className="row pt-3">
                        <div className="col-12"><h1>Colleagues you've met</h1></div>
                        {employees.map(employee => (
                            <div className="col-xl-4 mb-3" key={employee.id}>
                                <Employees avatar={employee.avatar} id={employee.id}
                                           first_name={employee.first_name}
                                           last_name={employee.last_name}
                                           metEmployee={metEmployee}
                                           hasMet={employee.hasMet}
                                           email={employee.email}/>
                            </div>
                        ))}
                    </div>
                </div>

            ) : (
                <h2 className="text-center">You haven't met any of your colleagues yet</h2>
            )}
        </div>
    )
}
const ConnectedMetEmployeesPage = props => {
    const dispatch = useDispatch();

    useEffect(() => {

    }, [])

    const employees = useSelector(state => state.employees)
    const isFetching = useSelector(state => state.isFetching)

    return <MetEmployeesPage employees={employees.filter(employee => employee.hasMet === true)}
                             isFetching={isFetching}/>
}
export default ConnectedMetEmployeesPage
