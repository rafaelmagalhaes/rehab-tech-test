import React, {useEffect} from 'react'
import {fetchSingleEmployee} from "../redux/actions";
import {useDispatch, useSelector} from 'react-redux'
import Employees from "../components/Employees";

const EmployeePage = props => {
    const {employee, isFetching} = props;
    return (
        <div>
            {
                isFetching || !employee ? (
                    <span>Loading...</span>
                ) : (
                    <div>
                        <Employees avatar={employee.avatar} id={employee.id}
                                   first_name={employee.first_name}
                                   hasMet={employee.hasMet}
                                   last_name={employee.last_name}
                                   email={employee.email}/>
                    </div>
                )
            }
        </div>
    )
}

const ConnectedEmployeePage = props => {
    const {match: {params: {id}}} = props;
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchSingleEmployee(id))
    }, [id])
    const employee = useSelector(state => state.employees.find(employee => employee.id === parseInt(id)))
    const isFetching = useSelector(state => state.isFetching)

    return <EmployeePage employee={employee} isFetching={isFetching}/>
}
export default ConnectedEmployeePage
