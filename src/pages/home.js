import React, {useEffect} from 'react'
import {useHistory} from "react-router-dom";
import {fetchEmployees, metEmployee} from "../redux/actions";
import Employees from "../components/Employees/"
import {useDispatch, useSelector} from "react-redux";
import Pagination from "../components/Pagination"
import queryString from 'query-string';

const HomePage = props => {
    const {employees, isFetching, metEmployee, paginationConfig, paginate} = props;
    return (
        <div className="App">
            {isFetching || !employees.length ? (
                <span>Loading...</span>
            ) : (
                <div className="container">
                    <div className="row pt-3">
                        {employees.map(employee => (
                            <div className="col-xl-4 mb-3" key={employee.id}>
                                <Employees avatar={employee.avatar} id={employee.id}
                                           first_name={employee.first_name}
                                           last_name={employee.last_name}
                                           metEmployee={metEmployee}
                                           hasMet={employee.hasMet}
                                           email={employee.email}/>
                            </div>
                        ))}
                        <div className="col-12">
                            <Pagination
                                postsPerPage={paginationConfig.perPage}
                                currentPage={paginationConfig.currentPage}
                                totalPosts={paginationConfig.total}
                                paginate={paginate}
                            />
                        </div>
                    </div>
                </div>
            )}
        </div>
    )
}
const ConnectedHomePage = props => {
    const dispatch = useDispatch();
    const {location: {search}} = props;
    useEffect(() => {
        dispatch(fetchEmployees(queryString.parse(search).page))
    }, []);

    const history = useHistory();
    const employees = useSelector(state => state.employees);
    const paginationConfig = {
        perPage: useSelector(state => state.perPage),
        currentPage: useSelector(state => state.employeePage),
        total: useSelector(state => state.totalEmployees)
    };
    const isFetching = useSelector(state => state.isFetching);

    function paginate(number) {
        dispatch(fetchEmployees(number));
        history.push({
            pathname: '/',
            search: `?page=${number}`
        })
        paginationConfig.currentPage = number
    }

    return <HomePage employees={employees} isFetching={isFetching} metEmployee={(id) => dispatch(metEmployee(id))}
                     paginate={paginate} paginationConfig={paginationConfig}/>
}
export default ConnectedHomePage
