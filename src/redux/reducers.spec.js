import employees from './reducer'

describe('Employees Reducers', () => {
    const initState = {
        isFetching: false,
        employees: [],
        employeePage: null,
        perPage: null,
        totalEmployees: null,
        employee: {},
    };

    it('returns the initial state when actions is not passed', () => {
        const reducer = employees(undefined, {});
        expect(reducer).toEqual(initState)
    })
    it('isFetching should be true when action REQUEST_EMPLOYEES is called', () => {
        const reducer = employees(initState, {type: "REQUEST_EMPLOYEES"});
        expect(reducer).toEqual({
            isFetching: true,
            employees: [],
            employeePage: null,
            perPage: null,
            totalEmployees: null,
            employee: {},
        });
    });
})
