import {
    RECEIVE_EMPLOYEES,
    RECEIVE_SINGLE_EMPLOYEE,
    REQUEST_EMPLOYEES,
    REQUEST_SINGLE_EMPLOYEE,
    Met_Employee
} from "./actions";
import {persistReducer} from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web

const persistConfig = {
    key: 'employees',
    storage,
}
const employees = (
    state = {
        isFetching: false,
        employees: [],
        employeePage: null,
        perPage: null,
        totalEmployees: null,
        employee: {},
    },
    action
) => {
    switch (action.type) {
        case REQUEST_EMPLOYEES:
            return Object.assign({}, state, {
                isFetching: true
            });
        case REQUEST_SINGLE_EMPLOYEE:
            return Object.assign({}, state, {
                isFetching: true
            });
        case RECEIVE_EMPLOYEES:
            return Object.assign({}, state, {
                isFetching: false,
                employees: mergeById(action.employees.data, state.employees),
                perPage: action.employees.per_page,
                totalEmployees: action.employees.total,
                employeePage: action.employees.page
            });
        case RECEIVE_SINGLE_EMPLOYEE:
            return Object.assign({}, state, {
                isFetching: false,
                employees: mergeById([action.employee], state.employees)
            });
        case Met_Employee:
            return Object.assign({}, state, {
                employees: mergeById(state.employees, [{id: action.id, hasMet: true}])
            })
        default:
            return state;
    }
};

const mergeById = (a1, a2) => a1.map(itm => ({...a2.find((item) => (item.id === itm.id) && item), ...itm}));
export default persistReducer(persistConfig, employees);
