export const REQUEST_EMPLOYEES = "REQUEST_EMPLOYEES";

function requestEmployees() {
    return {
        type: REQUEST_EMPLOYEES
    };
}

//request singe employee
export const REQUEST_SINGLE_EMPLOYEE = "REQUEST_SINGLE_EMPLOYEE";

function requestSingleEmployee() {
    return {
        type: REQUEST_SINGLE_EMPLOYEE
    };
}

export const RECEIVE_EMPLOYEES = "RECEIVE_EMPLOYEES";

function receiveEmployees(data) {
    return {
        type: RECEIVE_EMPLOYEES,
        employees: data
    };
}

export const Met_Employee = 'Met_Employee';

export function metEmployee(id) {
    return {
        type: Met_Employee,
        id
    }
}

//set single employee
export const RECEIVE_SINGLE_EMPLOYEE = "RECEIVE_SINGLE_EMPLOYEE";

function receiveSingleEmployee(data) {
    return {
        type: RECEIVE_SINGLE_EMPLOYEE,
        employee: data
    };
}

export function fetchEmployees(pageNum) {
    return function (dispatch,getState) {
        dispatch(requestEmployees());
        let url = "https://reqres.in/api/users?per_page=6&delay=2";
        if(pageNum){
            url =`https://reqres.in/api/users?per_page=6&page=${pageNum}`
        }
        return fetch(url)
            .then(
                response => response.json(),
                error => console.log("An error occurred.", error)
            )
            .then(json => {
                console.log(getState().employees)
                dispatch(receiveEmployees(json))
            });
    };
}

//fetch single employee
export function fetchSingleEmployee(id) {
    return function (dispatch, getState) {
        dispatch(requestSingleEmployee())
        return fetch(`https://reqres.in/api/users/${id}`)
            .then(
                response => response.json(),
                error => console.log("An error occurred.", error)
            )
            .then(json => dispatch(receiveSingleEmployee(json.data)));

    };
}
