import React from 'react';
import {NavLink} from 'react-router-dom';

import './nav.css';

const nav = props => (
        <header className=" fixed-top bg-dark  py-3">
            <div className="row flex-nowrap justify-content-between align-items-center">
                <div className="col-8 offset-2 d-flex justify-content-center align-items-center">
                    <NavLink to="/" className="text-white"><img
                        style={{height:20+'px'}}
                        src="https://uploads-ssl.webflow.com/5cb1c94b2da2ada2a553ab3f/5cb1ca2a46fa449c04bd168b_all%20light.svg"
                        alt=""/>
                    </NavLink>
                </div>
                <div className="col-2 d-flex justify-content-end  align-items-center">
                    <NavLink to="/" className="text-white mr-4">Home</NavLink>
                    <NavLink to="/met-employee" className="text-white mr-3">Colleagues you've met</NavLink>
                </div>
            </div>
        </header>);

export default nav;
