import React from 'react'
import {
    Link,
} from 'react-router-dom'
import './employee.css'

function Index({first_name, last_name, email, avatar, id, metEmployee, hasMet}) {
    const location = window.location.pathname;
    return (

        <div className="employee__Cards">
            <div className="card mb-4 mt-3">
                <img alt="" className="card-img-top m-auto" src={avatar}/>
                <hr/>
                <div className="card-body">
                    {location.includes('employee') && <h2>{first_name} {last_name} </h2>}
                    {hasMet && location.includes('employee') && <span style={{fontSize: 20 + 'px'}}> You've met this person </span>}
                    {!location.includes('employee') && <Link to={`/employee/${id}`}
                                                             className="employee__cards__href card-title">{first_name} {last_name}</Link>}
                    <p className="card-text">{email}</p>
                    {!location.includes('employee') && !hasMet &&
                    <button onClick={() => metEmployee(id)}>Met {first_name} {last_name}</button>
                    }

                </div>
            </div>
        </div>
    )
}

export default Index
