import React from "react";
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import './assets/style/main.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Nav from './components/Nav'

import HomePage from './pages/home'
import EmployeePage from './pages/employee'
import MetEmployeePage from './pages/met-employees'

class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <React.Fragment>
                    <Nav/>
                    <main className="main-content">
                        <Switch>
                            <Route exact path="/" component={HomePage}/>
                            <Route path="/employee/:id" component={EmployeePage}/>
                             <Route path="/met-employee" component={MetEmployeePage}/>
                        </Switch>
                    </main>
                </React.Fragment>
            </BrowserRouter>
        )
    }
}

export default App
